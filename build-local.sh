#/bin/bash

docker rm -f app

docker build -t app:latest .

docker run -d --name app -p 5000:5000 app:latest